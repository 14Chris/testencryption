namespace TestEncryptionEF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class init_co : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CompteUtilisateur",
                c => new
                    {
                        id_compte_user = c.Int(nullable: false, identity: true),
                        id_personne = c.Int(nullable: false),
                        login = c.String(nullable: false, maxLength: 100, unicode: false),
                        password = c.String(nullable: false, maxLength: 5000, unicode: false),
                    })
                .PrimaryKey(t => t.id_compte_user)
                .ForeignKey("dbo.Personne", t => t.id_personne)
                .Index(t => t.id_personne);
            
            CreateTable(
                "dbo.Personne",
                c => new
                    {
                        id_personne = c.Int(nullable: false, identity: true),
                        nom = c.String(),
                        prenom = c.String(),
                        date_naissance = c.String(),
                    })
                .PrimaryKey(t => t.id_personne);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CompteUtilisateur", "id_personne", "dbo.Personne");
            DropIndex("dbo.CompteUtilisateur", new[] { "id_personne" });
            DropTable("dbo.Personne");
            DropTable("dbo.CompteUtilisateur");
        }
    }
}
