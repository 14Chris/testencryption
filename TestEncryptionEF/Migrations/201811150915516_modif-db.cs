namespace TestEncryptionEF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class modifdb : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.CompteUtilisateur", "login", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.CompteUtilisateur", "password", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.CompteUtilisateur", "password", c => c.String(nullable: false, maxLength: 5000, unicode: false));
            AlterColumn("dbo.CompteUtilisateur", "login", c => c.String(nullable: false, maxLength: 100, unicode: false));
        }
    }
}
