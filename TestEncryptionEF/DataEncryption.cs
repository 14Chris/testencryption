﻿using System;
using System.IO;
using System.Security.Cryptography;

namespace TestEncryptionEF
{

    public class DataEncryption
    {
        /// <summary>
        /// Fonction utilisant AES 256 pour chiffrer une chaîne de caratères
        /// </summary>
        /// <param name="data">Donnée à chiffrer</param>
        /// <returns></returns>
        public static String EncryptData(String data)
        {
            byte[] res = EncryptStringToBytes_Aes(data, GetAesKey(), GetAesIv());

            return Convert.ToBase64String(res);
        }

        /// <summary>
        /// Fonction utilisant AES 256 pour déchiffrer une chaîne de caratères
        /// </summary>
        /// <param name="data">Donnée chiffrée</param>
        /// <returns></returns>
        public static String DecryptData(byte[] data)
        {
            String res = DecryptStringFromBytes_Aes(data, GetAesKey(), GetAesIv());

            return res;
        }

        private static byte[] EncryptStringToBytes_Aes(string plainText, byte[] Key, byte[] IV)
        {
            // Check arguments.
            if (plainText == null || plainText.Length <= 0)
                throw new ArgumentNullException("plainText");
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("IV");
            byte[] encrypted;

            // Create an AesManaged object
            // with the specified key and IV.
            using (AesManaged aesAlg = new AesManaged())
            {
                aesAlg.KeySize = 256;
                aesAlg.BlockSize = 128;
                aesAlg.Key = Key;
                aesAlg.IV = IV;

                // Create a decrytor to perform the stream transform.
                ICryptoTransform encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);

                // Create the streams used for encryption.
                using (MemoryStream msEncrypt = new MemoryStream())
                {
                    using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                        {
                            //Write all data to the stream.
                            swEncrypt.Write(plainText);
                        }
                        encrypted = msEncrypt.ToArray();
                    }
                }

                // Return the encrypted bytes from the memory stream.
                return encrypted;
            }
        }

        private static string DecryptStringFromBytes_Aes(byte[] cipherText, byte[] Key, byte[] IV)
        {
            // Check arguments.
            if (cipherText == null || cipherText.Length <= 0)
                throw new ArgumentNullException("cipherText");
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("IV");

            // Declare the string used to hold
            // the decrypted text.
            string plaintext = null;

            // Create an AesManaged object
            // with the specified key and IV.
            using (AesManaged aesAlg = new AesManaged())
            {
                aesAlg.Key = Key;
                aesAlg.IV = IV;

                // Create a decrytor to perform the stream transform.
                ICryptoTransform decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);

                // Create the streams used for decryption.
                using (MemoryStream msDecrypt = new MemoryStream(cipherText))
                {
                    using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader srDecrypt = new StreamReader(csDecrypt))
                        {

                            // Read the decrypted bytes from the decrypting stream
                            // and place them in a string.
                            plaintext = srDecrypt.ReadToEnd();
                        }
                    }
                }

            }

            return plaintext;
        }

        /// <summary>
        /// Fonction récupérant la clé SSL et la convertit en tableau d'octet
        /// </summary>
        /// <returns></returns>
        private static byte[] GetAesKey()
        {
            //TODO Chemin de la clé ssl
            //StreamReader s = new StreamReader(@"C:\Users\Chris\Source\Repos\TestEncryptionEF\key.dat");
            StreamReader s = new StreamReader(@"C:\Users\Corentin\Source\Repos\TestEncryption\key.dat");

            String hexKey = s.ReadToEnd();

            return NetUtility.ToByteArray(hexKey);
        }

        /// <summary>
        /// Fonction récupérant l'iv SSL et la convertit en tableau d'octet
        /// </summary>
        /// <returns></returns>
        private static byte[] GetAesIv()
        {
            //TODO Chemin de l'iv ssl
            //StreamReader s = new StreamReader(@"C:\Users\Chris\Source\Repos\TestEncryptionEF\iv.dat");
            StreamReader s = new StreamReader(@"C:\Users\Corentin\Source\Repos\TestEncryption\iv.dat");

            String hexKey = s.ReadToEnd();

            return NetUtility.ToByteArray(hexKey);
        }

    }

    public static class NetUtility
    {

        /// <summary>
        /// Convert a hexadecimal string to a byte array
        /// </summary>
        public static byte[] ToByteArray(String hexString)
        {
            byte[] retval = new byte[hexString.Length / 2];
            for (int i = 0; i < hexString.Length; i += 2)
                retval[i / 2] = Convert.ToByte(hexString.Substring(i, 2), 16);
            return retval;
        }

    }

}

