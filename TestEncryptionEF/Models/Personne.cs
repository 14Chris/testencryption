using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;


namespace TestEncryptionEF
{

    using static DataEncryption;

    [Table("Personne")]
    [DataContract(IsReference = true)]
    public partial class Personne
    {
        public Personne()
        {
            CompteUtilisateur = new List<CompteUtilisateur>();
        }

        [Key]
        [DataMember]
        public int id_personne { get; set; }

        [Required]
        [StringLength(100)]
        [DataMember]
        [NotMapped]
        public string nom
        {
            get
            {
                if (!String.IsNullOrEmpty(prenom_encrypted))
                    return DecryptData(Convert.FromBase64String(nom_encrypted));
                else
                    return "";
            }

            set
            {
                nom_encrypted = EncryptData(value);
            }
        }

        [Required]
        [StringLength(100)]
        [DataMember]
        [NotMapped]
        public string prenom
        {
            get
            {
                if (!String.IsNullOrEmpty(prenom_encrypted))
                    return DecryptData(Convert.FromBase64String(prenom_encrypted));
                else
                    return "";
            }

            set
            {
                prenom_encrypted = EncryptData(value);
            }
        }

        [Required]
        [DataMember]
        [NotMapped]
        public DateTime date_naissance
        {
            get
            {
                if (!String.IsNullOrEmpty(date_naissance_encrypted))
                    return Convert.ToDateTime(DecryptData(Convert.FromBase64String(date_naissance_encrypted)));
                else
                    return new DateTime();
            }

            set
            {
                date_naissance_encrypted = EncryptData(value.ToString());
            }
        }

        [DataMember]
        public List<CompteUtilisateur> CompteUtilisateur { get; set; }


        [Column("nom") ]
        public String nom_encrypted { get; set; }

        [Column("prenom")]
        public String prenom_encrypted { get; set; }

        [Column("date_naissance")]
        public String date_naissance_encrypted { get; set; }



    }
}
