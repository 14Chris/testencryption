﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace TestEncryptionEF
{
    // REMARQUE : vous pouvez utiliser la commande Renommer du menu Refactoriser pour changer le nom de classe "Service1" dans le code, le fichier svc et le fichier de configuration.
    // REMARQUE : pour lancer le client test WCF afin de tester ce service, sélectionnez Service1.svc ou Service1.svc.cs dans l'Explorateur de solutions et démarrez le débogage.
    public class Service1 : IService1
    {
        EncryptionModel db = new EncryptionModel();

        public int AddPersonne(string nom, string prenom, DateTime dateNaiss)
        {

            try
            {
                Personne p = new Personne();

                p.nom = nom;
                p.prenom = prenom;
                p.date_naissance = dateNaiss;

                db.Personne.Add(p);

                db.SaveChanges();

                return p.id_personne;
            }
            catch(Exception ex)
            {
                return -1;
            }


        }

        public List<Personne> GetPersonneByName(string nom)
        {
            try
            {
                List<Personne> p = db.Personne.ToList().Where(x => x.nom.ToLower() == nom.ToLower() || x.prenom.ToLower() == nom.ToLower()).ToList();

                return p;
            }
            catch(Exception ex)
            {
                return null;
            }
        }

        public Personne GetPersonneById(int id)
        {
            try
            {
                return db.Personne.Where(a => a.id_personne == id).SingleOrDefault();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<Personne> GetPersonnes()
        {
            return db.Personne.ToList();
        }
    }
}
